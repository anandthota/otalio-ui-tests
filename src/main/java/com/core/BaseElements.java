package com.core;

import com.utilities.ThrowException;
import org.openqa.selenium.*;
import org.openqa.selenium.support.ui.ExpectedConditions;

import static com.core.DriverManager.*;


public class BaseElements {

    public static void send_keys_to_element(By locator, String data)  {
        try {
            waitForElementToBeClickable(locator);
            driver.findElement(locator).clear();
            driver.findElement(locator).sendKeys(data);
        }
        catch(TimeoutException timeoutException)
        {
           throw new ThrowException("TimeoutException", timeoutException);
        }
        catch(NoSuchElementException noSuchElementException)
        {
            throw new ThrowException("No Such Element exception");
        }
        catch (Exception e)
        {
            throw new ThrowException("TimeoutException", e);

        }
    }

    public static void click_on_element(By locator)
    {
       try {
           try {
               waitForElementToBeClickable(locator);
               driver.findElement(locator).click();
           } catch (StaleElementReferenceException staleElementReferenceException) {
               waitForElementToBeClickable(locator);
               driver.findElement(locator).click();
           }
       }
       catch(TimeoutException timeoutException)
       {
           throw new ThrowException("No Such Element exception", timeoutException);
       }
       catch(NoSuchElementException noSuchElementException)
       {
           throw new ThrowException("No such", noSuchElementException);
       }
       catch (StaleElementReferenceException staleElementReferenceException) {
           waitForElementToBeClickable(locator);
           driver.findElement(locator).click();
       }
       catch(Exception e)
       {
           throw new ThrowException("exception", e);
       }
    }

    public static String get_data_element(By locator) {
        waitForElementToBeClickable(locator);
        return driver.findElement(locator).getText();
    }

    public static void waitForElementToBeClickable(By locator)
    {
        webDriverWait.until(ExpectedConditions.elementToBeClickable(locator));
    }

    public static Boolean checkElementIsInvisible(By locator)
    {
        try{
            driver.findElement(locator).isDisplayed();
            return false;
        }
            catch (NoSuchElementException e)
        {
            return true;
        }
    }

    public static void waitForElementToBeVisible(By locator)
    {
        WebElement element = driver.findElement(locator);
        webDriverWait.until(ExpectedConditions.visibilityOf(element));
    }

    public static void check_current_url(String url)
    {
        webDriverWait.until(ExpectedConditions.urlContains(url));
    }

    public static void scrollToElement(By locator)
    {
        javascriptExecutor.executeScript("arguments[0].scrollIntoView(true)", driver.findElement(locator));
    }

    public static void scrollTo(By locator)
    {
        WebElement element = driver.findElement(locator);
        Point location = element.getLocation();
        javascriptExecutor.executeScript("window.scrollTo(" + location.getX() + "," + location.getY() + ");", element);
    }

    public static void click_on_element_in_table(By locator)
    {
        try {
            try {
                waitForElementToBeClickable(locator);
                scrollToElement(locator);
                driver.findElement(locator).click();
            } catch (StaleElementReferenceException staleElementReferenceException) {
                waitForElementToBeClickable(locator);
                driver.findElement(locator).click();
            }
        }
        catch(TimeoutException timeoutException)
        {
            throw new ThrowException("No Such Element exception", timeoutException);
        }
        catch(NoSuchElementException noSuchElementException)
        {
            throw new ThrowException("No such", noSuchElementException);
        }
        catch (StaleElementReferenceException staleElementReferenceException) {
            waitForElementToBeClickable(locator);
            driver.findElement(locator).click();
        }
        catch(Exception e)
        {
            throw new ThrowException("exception", e);
        }
    }

}
