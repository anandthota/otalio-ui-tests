package com.core;

import com.utilities.ConfigFile;
import io.cucumber.java.Scenario;
import org.apache.commons.io.FileUtils;
import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.OutputType;
import org.openqa.selenium.TakesScreenshot;

import static com.core.DriverManager.driver;



public class BasePage {

    public BasePage()
    {

    }

    public void openBrowser() {
        delete_all_cookies();
        driver.get(ConfigFile.hostName);
    }

    public void navigateToUrl(String url)
    {
        driver.navigate().to(url);
    }

    public void delete_all_cookies()
    {
        driver.manage().deleteAllCookies();
    }

    public static void quitBrowser()
    {
        driver.quit();
    }

    public void closeTab()
    {
        driver.close();
    }

    public void refreshPage()
    {
        driver.navigate().refresh();
    }

    public String get_current_page_url()
    {
        return driver.getCurrentUrl();
    }

    public static void takeScreenshot(Scenario scenario)  {
        try {
            TakesScreenshot takesScreenshot = (TakesScreenshot) driver;
            final byte[] screenshot = takesScreenshot.getScreenshotAs(OutputType.BYTES);
           // File target = new File(projectParentDirectory + "/target/screenshot/.png");
           // FileUtils.copyFile(src, target);
            scenario.attach(screenshot, "image/png", scenario.getName());

           // scenario.attach(src, "image/png", "error");
        }
        catch(Exception e)
        {

        }
    }

}
