package com.core;

import com.utilities.ConfigFile;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.support.ui.WebDriverWait;

import java.io.File;
import java.time.Duration;
import java.util.concurrent.TimeUnit;

public class DriverManager {

    File file = new File("");
    String driverPath = file.getAbsolutePath() + "\\Drivers\\chromedriver.exe";
    public static WebDriver driver;
    public static JavascriptExecutor javascriptExecutor;
    public static WebDriverWait webDriverWait;

    public DriverManager(String browserName)
    {
        initializeDriver(browserName);
    }

    public WebDriver initializeDriver(String browser){

        switch (browser) {
            case "firefox":
            default:
                System.setProperty("webdriver.chrome.driver", driverPath);
                driver = new ChromeDriver();
                driver.manage().window().maximize();
                driver.manage().timeouts().implicitlyWait(Duration.ofSeconds(10));
                webDriverWait = new WebDriverWait(driver, Duration.ofSeconds(10));
                javascriptExecutor = (JavascriptExecutor) driver;
                return driver;
        }
    }

//    public static WebDriver getBrowser(){
//        return driver;
//    }
}
