package com.API;

import com.utilities.ConfigFile;
import io.restassured.RestAssured;
import io.restassured.response.Response;
import io.restassured.http.ContentType;
import models.LoginModelImpl;
import java.util.HashMap;

import static com.utilities.ConfigFile.*;
import static io.restassured.RestAssured.given;

public class RestAPICall {

    private static Response response;
    private static LoginModelImpl loginModel;

    public void setUser(String user) {
        RestAssured.baseURI = hostName;
        switch(user)
        {
            case "admin":
                break;
            case "invalid":
                userName = user + "@otalio.com";
                break;
            default:
                userName = user + "@otalio.com";
        }
        loginModel = new LoginModelImpl(userName, password);
    }


    public void getResponse(String uri)
    {
        response = given().baseUri(hostName)
            .contentType(ContentType.JSON)
            .when()
            .header("Authorization", "Bearer "+ getBearertoken())
            .queryParams("","")
            .get(uri)
            .then()
            .extract().response();
    }

    public String getBearertoken()
    {
        try {
             response = given().baseUri(hostName)
                    .header("Content-type", "application/json")
                    .and()
                    .body(loginModel.getLoginRequestPayload())
                    .when()
                    .post("/iam/v1/sso/login")
                    .then()
                    .extract().response();

            HashMap map = response.jsonPath().get("responsePayload");
            return map.get("access_token").toString();
        }
        catch(Exception e)
        {
            return "401unauthorized";
            //throw new ThrowException("provide right user and password");
        }
    }

    public void getPostresponse(String uri, String payload)  {
        System.out.println(payload);
        response = given().baseUri(hostName)
                .header("Content-type", "application/json")
                .header("Authorization", "Bearer "+ getBearertoken())
                .and()
                .body(payload)
                .when()
                .post(uri)
                .then()
                .extract().response();
    }

    public int getStatusCode() {
        return response.getStatusCode();
    }


    public String getPostResponseBody(String data)
    {
        return response.jsonPath().get(data);
    }
}
