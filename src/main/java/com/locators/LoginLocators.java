package com.locators;

import org.openqa.selenium.By;

public class LoginLocators {

    public By email_text_field = By.id("login-email-input");
    public By password_text_field = By.id("login-password-input");
    public By Login_button = By.id("login-login-button");
    public By user_profile_button = By.id("profile-keyboard_arrow-button");
    public By personal_information_button = By.cssSelector("[icon='personal-information']");
    public By user_name_data = By.xpath("//div[contains(@class,'user-avatar-outer')]/following::h1[1]");
    public By logout_button = By.id("profile-exit_to_app-logout-button");
}
