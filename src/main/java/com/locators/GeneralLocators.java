package com.locators;

import org.openqa.selenium.By;

public class GeneralLocators {

    public By locate_by_text(String data)
    {
        return (By.xpath("//*text()[contains(.,'" + data + "'])"));
    }
}
