package com.locators;

import org.openqa.selenium.By;

public class SecurityLocators {

    public By securityLink = By.id("layout-menu-link-security-a");
    public By typeDropdown = By.id("complex-filter-label-frontend-open-button");
    public By fronendCheckbox = By.id("complex-filter-switch-overlay-list-option-true");
    public By backendCheckbox = By.id("complex-filter-switch-overlay-list-option-false");
    public By resourceSaveButton = By.id("resources-editor-save-button");
    public By gridContainer = By.className("dx-gridbase-container");
    public By breadcrumbTitle = By.id("breadcrumbs-title");
    public By permissionsSaveButton = By.id("permission-editor-save-button");
    public By nameDropdown = By.id("complex-filter-label-name-open-button");
    public By nameInput = By.id("complex-filter-search-overlay-term-input");


    public By tablerow(int row)
    {
        return By.cssSelector("[aria-rowindex='"+ row +"'] [aria-colindex='1'] a");
    }

    public By getPageElement(int number) {
        return By.cssSelector("[aria-label='Page "+number+"']");
    }

    public By getPermissionNameElement(String permissonName)
    {
        return By.xpath("//span[text()='"+permissonName+"']");
    }
}
