package com.locators.pos.Management;

import org.openqa.selenium.By;

public class ArtilceLocators {

    public By posManagementLink = By.cssSelector("[href*='pos-management']");
    public By articleSetupLink = By.linkText("Article Setup");
    public By newButton = By.id("pos-article-setup-list-add-button");
    public By codeInputField = By.id("article-setup-editor-code-input");
    public By decsriptionTextFiled = By.id("description-en-input");
    public By artilceGroupDropdown = By.id("otalio-infinite-select-articleGroup-field");
    public By artilceGroupSearch = By.id("otalio-infinite-select-articleGroup-field-search");
    public By artilceSaveButton = By.id("article-setup-editor-save-button");
    public By codeDropdownButton = By.id("complex-filter-label-code-open-button");
    public By searchBar = By.id("complex-filter-search-overlay-term-input");
    public By revenueCenterLabel = By.id("article-setup-editor-revenue-center-categories-select");
    public By overlay = By.className("cdk-overlay-container");
    public By updateButton = By.id("article-setup-editor-update-button");
    public By loading = By.cssSelector(".dx-overlay.dx-state-invisible");
    public By deleteArticleButton = By.id("article-setup-editor-delete-button");
    public By deleteConfirmationButton = By.id("delete-confirmation-dialog-delete-button");

    public By selectArticleGroup(String articleGroupName) {
        return By.xpath("//*[@class='mat-option-text'] [contains(text(),'" + articleGroupName + "')]");
    }

    public By getArticleNameElement(String articleName)
    {
        return By.xpath("//span[contains(text(),'"+articleName+"')]");
    }

    public By select_revenue_center(String revenue_center)
    {
        return By.xpath("//div[contains(text(), '"+revenue_center+"')]/following::button[1]");
    }


}
