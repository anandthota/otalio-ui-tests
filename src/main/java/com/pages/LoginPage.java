package com.pages;

import com.core.BaseElements;
import com.core.BasePage;
import com.locators.LoginLocators;

import static com.core.BaseElements.*;

public class LoginPage extends BasePage {

    LoginLocators loginLocators;

    public LoginPage()
    {
        loginLocators = new LoginLocators();
    }

    public void enterAdminUserName(String user)
    {
        send_keys_to_element(loginLocators.email_text_field, user);
    }

    public void enter_password(String user_password)
    {
       send_keys_to_element(loginLocators.password_text_field, user_password);
    }

    public void click_on_login_button()
    {
        click_on_element(loginLocators.Login_button);
    }

    public void click_on_user_profile()
    {
       click_on_element(loginLocators.user_profile_button);
    }

    public void wait_for_user_profile_to_load()
    {
        waitForElementToBeClickable(loginLocators.user_profile_button);
    }

    public void click_on_personal_information_button()
    {
       click_on_element(loginLocators.personal_information_button);
    }

    public String get_user_loged_in_name()
    {
        return get_data_element(loginLocators.user_name_data);
    }

    public void click_on_logout_button()
    {
        click_on_element(loginLocators.logout_button);
    }

    public void login_as_admin(String user, String pswd) {
        enterAdminUserName(user);
        enter_password(pswd);
        click_on_login_button();
    }
}
