package com.pages.pos;

import com.locators.pos.Management.ArtilceLocators;

import static com.core.BaseElements.*;


public class ArticleSetupPage  {

    ArtilceLocators artilceLocators = new ArtilceLocators();

    public void clickOnPOSManagement()
    {
        click_on_element(artilceLocators.posManagementLink);
    }

    public void clickOnArtilceSetup()
    {
        click_on_element(artilceLocators.articleSetupLink);
    }

    public void clickOnNewArticleButton()
    {
        click_on_element(artilceLocators.newButton);
    }

    public void enterArticleName(String articleName)
    {

        send_keys_to_element(artilceLocators.codeInputField, articleName);
    }

    public void enterArticleDescription(String articleDescription)
    {
        send_keys_to_element(artilceLocators.decsriptionTextFiled, articleDescription);
    }

    public void selectArticleGroup(String artilceGroupName)
    {
        click_on_element(artilceLocators.artilceGroupDropdown);
        click_on_element(artilceLocators.artilceGroupSearch);
        send_keys_to_element(artilceLocators.artilceGroupSearch, artilceGroupName);
        click_on_element(artilceLocators.selectArticleGroup(artilceGroupName));
    }

    public void clickOnCreateButton()
    {
        click_on_element(artilceLocators.artilceSaveButton);
    }

    public void clickOnCodeDropdown()
    {
        click_on_element(artilceLocators.codeDropdownButton);
    }

    public void enterCodeOnArticleSetupLandingPage(String articleCode)
    {
        send_keys_to_element(artilceLocators.searchBar, articleCode);
    }

    public String getArticleText(String articleCode)
    {
        return get_data_element(artilceLocators.getArticleNameElement(articleCode.toUpperCase()));
    }

    public void click_on_revenue_center() {
        click_on_element(artilceLocators.revenueCenterLabel);
    }

    public void select_revenue_center_category(String revenue_center) {
        click_on_element(artilceLocators.select_revenue_center(revenue_center));
    }

    public void clickOnArticle(String articleCode) {
        click_on_element(artilceLocators.getArticleNameElement(articleCode.toUpperCase()));
    }

    public void closeOverlay() {
        click_on_element(artilceLocators.overlay);
    }

    public void clickOnUpdateButton() {
        click_on_element((artilceLocators.updateButton));
    }

    public void waitForArticleToLoad(String articleCode) {
        waitForElementToBeVisible(artilceLocators.getArticleNameElement(articleCode.toUpperCase()));
    }

    public void clickOnDeleteArtilce() {
        click_on_element(artilceLocators.deleteArticleButton);
        click_on_element(artilceLocators.deleteConfirmationButton);
    }

    public boolean articleNotDisaplayed(String articleName) {
        return checkElementIsInvisible(artilceLocators.getArticleNameElement(articleName.toUpperCase()));
    }
}
