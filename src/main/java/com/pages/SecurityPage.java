package com.pages;

import com.locators.SecurityLocators;

import static com.core.BaseElements.*;

public class SecurityPage {

    SecurityLocators securityLocators = new SecurityLocators();

    public void clickOnResource(int row, int count) {
       // if(count < 11) {
            try {
                click_on_element(securityLocators.tablerow(row));
            }
            catch (Exception e)
            {
                click_on_element_in_table(securityLocators.tablerow(row));
            }
//        }
//        else
//            click_on_element_in_table(securityLocators.tablerow(row));
    }

    public void clickOnTypeFronend() {
        click_on_element(securityLocators.typeDropdown);
        click_on_element(securityLocators.fronendCheckbox);
    }

    public void clickOnApplicationSecurity() {
        click_on_element(securityLocators.securityLink);
    }

    public void clickOnSave() {
        click_on_element(securityLocators.resourceSaveButton);
    }

    public void clickOnPage(int number) {
        click_on_element(securityLocators.getPageElement(number));
    }

    public void scrollToTableGrid() {
        scrollToElement(securityLocators.gridContainer);
    }

    public String getResourceName() {
        return get_data_element(securityLocators.breadcrumbTitle);
    }

    public String getResourceNameOnLandingPage(int row)
    {
       return get_data_element(securityLocators.tablerow(row));
    }


    public void clickOnPermissoinsSave() {
        click_on_element(securityLocators.permissionsSaveButton);
    }

    public void clickOnNameDropdown() {
        click_on_element(securityLocators.nameDropdown);
    }

    public void enterDataToPermissionName(String permissionNameOnLandingPage) {
        click_on_element(securityLocators.nameInput);
        send_keys_to_element(securityLocators.nameInput, permissionNameOnLandingPage);
    }

    public void clickOnPermissionName(String permissionNameOnLandingPage) {
        click_on_element(securityLocators.getPermissionNameElement(permissionNameOnLandingPage));
    }
}
