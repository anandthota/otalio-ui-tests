package com.utilities;

import ws.schild.jave.Encoder;
import ws.schild.jave.MultimediaObject;
import ws.schild.jave.encode.EncodingAttributes;
import ws.schild.jave.encode.VideoAttributes;
import ws.schild.jave.encode.enums.X264_PROFILE;
import ws.schild.jave.info.VideoSize;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;

public class VedioConverter {

    File source ;
    File target ;
    VideoAttributes video;
    EncodingAttributes attrs;

    public String getConvertedvideo(String src, String target1)  {
        try {

            source = new File(src);
            target = new File(target1);
            setAudioAttributes();
            setVedioAttributes();
            encodeAttributes();
            doEncoding();
        }
        catch (Exception e)
        {
            e.printStackTrace();
        }
        finally {

        }
        {
            return target1;
        }
    }

    public void setAudioAttributes(){
        /* Step 2. Set Audio Attrributes for conversion*/
//    AudioAttributes audio = new AudioAttributes();
//    audio.setCodec("aac");
//    // here 64kbit/s is 64000
//    audio.setBitRate(64000);
//    audio.setChannels(2);
//    audio.setSamplingRate(44100);

    }

    public void setVedioAttributes() throws FileNotFoundException {
        video = new VideoAttributes();
        video.setCodec("mpeg4");
        //video.setX264Profile(X264_PROFILE.HIGH);
// Here 160 kbps video is 160000
        video.setBitRate(160000);
 //More the frames more quality and size, but keep it low based on devices like mobile
        video.setFrameRate(10);
        video.setSize(new VideoSize(400,300));

    }

    public void encodeAttributes()
    {
        attrs = new EncodingAttributes();
        attrs.setOutputFormat("mp4");
        //attrs.setFormat("mp4");
        //attrs.setAudioAttributes(audio);
        attrs.setVideoAttributes(video);
    }

    /* Step 4. Set Encoding Attributes*/

    public void doEncoding(){
        try
        {
            Encoder encoder = new Encoder();
            encoder.encode(new MultimediaObject(source), target, attrs);
        } catch( Exception e)
        {
            /*Handle here the video failure*/
            e.printStackTrace();
        }

    }
        /* Step 5. Do the Encoding*/
//    public static void main(String args[]) throws FileNotFoundException {
//        VedioConverter vedioConverter = new VedioConverter();
//    }
}
