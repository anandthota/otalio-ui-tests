package com.utilities;


import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;

public class GenerateJsonUsingSerilization {

    private String requestPayload;

    public GenerateJsonUsingSerilization(Object object) {
        ObjectMapper objectMapper = new ObjectMapper();
        try {
            requestPayload = objectMapper.writerWithDefaultPrettyPrinter().writeValueAsString(object);
        } catch (JsonProcessingException e) {
        }
    }


    public String getRequestPayload() {
        return requestPayload;
    }


}
