package com.utilities;

import com.API.RestAPICall;
import com.core.DriverManager;

import java.io.*;
import java.util.HashMap;
import java.util.Map;
import java.util.Properties;

public class ConfigFile {

    private static Properties readProperties;
    public static String userName;
    public static String password;
    public static String hostName;
    public static String browserName;
    public static String projectParentDirectory;
    public static RestAPICall restAPICall;
    public static String articleName;
    static File file;

    public static void initialize()
    {
        file = new File("");
        projectParentDirectory = file.getAbsolutePath();
        readProperties = new Properties();
        try {
            readProperties.load(new FileInputStream(projectParentDirectory + "\\src\\config\\env.properties"));
        } catch (IOException e) {
            e.printStackTrace();
        }
        setUserNameAndPassword();
        setEnvironment();
        setBrowser();
        //restAPICall = new RestAPICall();
    }


    public static void setUserNameAndPassword() {
                userName = readProperties.getProperty("admin_user");
                password = readProperties.getProperty("admin_password");
    }

    public static void setEnvironment() {
        hostName = readProperties.getProperty("env");
    }

    public static void setBrowser() {
        browserName = readProperties.getProperty("browser");
    }

    public static void setDriver(String browser) {
        DriverManager driverManager = new DriverManager(browser);
    }

//    public static void  main(String args[])
//    {
//        initialize();
//        Properties properties = System.getProperties();
//        Map<String, String> map = new HashMap<>();
//        for (final String name : properties.stringPropertyNames()){
//            if (name.equals("browser"))
//                System.out.println(name + ":" +properties.getProperty(name).trim());
//        }
//        System.out.println(System.getProperty("browser"));
//    }
}
