package com.utilities;

import java.util.Map;
import java.util.Random;
import java.util.stream.Collectors;

public class DataUtils {

    public static String replacePlaceholderStrings(String value)
    {
        return value.replaceAll("random", generateRandomNumber());
    }

    public static String generateRandomNumber()
    {
        Random random = new Random();
        return random.nextInt(999999)+"";
    }

    public static Map<String, String> updatePlaceholderStringsOfMap(Map<String, String> map) {
        return map.keySet().stream().collect(Collectors.toMap(key -> key, key -> replacePlaceholderStrings(map.get(key))));
    }

//    public static void main(String a[])
//    {
//        System.out.println(generateRandomNumber());
//    }

}
