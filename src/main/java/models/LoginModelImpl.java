package models;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.utilities.GenerateJsonUsingSerilization;

public class LoginModelImpl {


    private String LoginRequestPayload;

    public LoginModelImpl(String user, String password)
    {
        LoginModel loginModel = new LoginModel(user, password);
        GenerateJsonUsingSerilization generateJsonUsingSerilization = new GenerateJsonUsingSerilization(loginModel);
        LoginRequestPayload = generateJsonUsingSerilization.getRequestPayload();
    }

    public String getLoginRequestPayload() {
        return LoginRequestPayload;
    }
}


