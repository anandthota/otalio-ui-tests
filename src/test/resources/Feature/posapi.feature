Feature: POSAPI

  @api
  Scenario Outline: Verify posAPI reponse code for get calls without query params
    Given I generate token for "<user>"
    Given I request "<api_method>" call "pos/v1/article-categories" for pos article-categoires
    Then I verify response_code as "<response_status>"
    Then I verify "<key>" response "<value>"

    Examples:

      | api_method | user    | response_status | key          | value                         |
      | get        | invalid | 401             | error.status | Unauthorized                  |
      | get        | admin   | 200             | message      | Records fetched successfully. |



