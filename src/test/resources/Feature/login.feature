Feature: login test

  @api
  Scenario Outline: verify <api_method> for sso_login file with <user> returns response as <response_status>
    Given I generate token for "<user>"
    Given I request "<api_method>" call "<URI>" for iam_sso_login
#    Then I verify response_code as "<response_status>"
#    Then I verify "message" response "<data>"
    Examples:
      | api_method | URI                | user    | response_status | data                                                  |
      | post       | /iam/v1/sso/login/ | invalid | 401             | Wrong user credentials during SignIn!                          |
      | post       | /iam/v1/sso/login/ | admin   | 200             | Login success                                                  |
      | post       | /iam/v1/sso/login/ | noacess | 401             | User requires active User Group assignment in order to log-in! |


  Scenario: verify user able to login with valid credentials
    Given I Open a Browser
    When I enter a user admin
    And I enter a user password
    And I click on login button
    And I click on user profile
    And I click on personal information
    Then I verify user displayed is Super User



