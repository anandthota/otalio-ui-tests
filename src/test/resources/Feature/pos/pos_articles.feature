@regression
Feature: Pos Articles crud

  #@article_groups @age_groups @debit_sub_departments @article_class @article_price
  @articles @clear_article
  Scenario: Create article
    Given I login Otalio as user admin
    When I click on "pos Management" module
    And I click on "Article Setup" link
    And I click on new button
    And I set values for article setup creation
      |Code | auto-article-random |
      | Description | auto-description-random |
      | Atricle Group | AB-Classic COCKTAILS |
    And I select "Bars" Revenue Center Category
    And I click on Create button
    And I search for article
    And I verify article is "created"

  @articles @clear_article
  Scenario: update article
    Given I login Otalio as user admin
    When I click on "pos Management" module
    And I click on "Article Setup" link
    And I click on new button
    And I set values for article setup creation
      |Code | auto-article-random |
      | Description | auto-description-random |
      | Atricle Group | AB-Classic COCKTAILS |
    And I select "Bars" Revenue Center Category
    And I click on Create button
    And I search for article
    And I verify article is "created"
    And I click on article "auto-article-random"
    And I wait for article "auto-article-random" loaded
    And I set values for article setup creation
      |Code | auto-article-random |
      | Description | auto-description-random |
      | Atricle Group | AB-Classic COCKTAILS |
    And I click on Create button
    And I click on Update button
    And I search for article
    Then I verify article is "updated"

  @articles
  Scenario: Delete article
    Given I login Otalio as user admin
    When I click on "pos Management" module
    And I click on "Article Setup" link
    And I click on new button
    And I set values for article setup creation
      |Code | auto-article-random |
      | Description | auto-description-random |
      | Atricle Group | AB-Classic COCKTAILS |
    And I select "Bars" Revenue Center Category
    And I click on Create button
    And I search for article
    And I verify article is "created"
    And I search for article
    And I click on article "auto-article-random"
    And I delete article "auto-article-random"
    Then I verify article is deleted

  Scenario: Save all resources
    Given I login Otalio as user admin
    And click on resources "1611" on pages "81" and save with type as Frontend

  Scenario: Save all permissions
    Given I login Otalio as user admin
    And click on permissions "1058" on pages "52" and save with type as Frontend





