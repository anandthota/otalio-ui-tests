
import io.cucumber.testng.AbstractTestNGCucumberTests;
import io.cucumber.testng.CucumberOptions;
import org.testng.annotations.DataProvider;


//@RunWith(Cucumber.class)
@CucumberOptions(
        features = "src/test/resources/Feature",
        glue ="stepDefinition",
        plugin = {"pretty", "html:target/cucumber-html-report","json:target/cucumber.json", "junit:target/cucumber-reports/Cucumber.xml"}
)
public class RunTest extends AbstractTestNGCucumberTests {

//    @DataProvider(parallel = false)
//    @Override
//    public Object[][] scenarios() {
//        return super.scenarios();
//    }
}