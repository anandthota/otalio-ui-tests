package stepDefinition.hooks;

import atu.testrecorder.ATUTestRecorder;
import com.utilities.ConfigFile;
import com.utilities.ThrowException;
import com.utilities.VedioConverter;
import io.cucumber.java.After;
import io.cucumber.java.Before;
import io.cucumber.java.Scenario;

import java.io.File;
import java.io.IOException;
import java.nio.charset.StandardCharsets;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;

import static com.core.BasePage.*;


public class Generalhooks {

    ATUTestRecorder recorder;
    String videoPath;
    String fileName;

    public Generalhooks() {
        ConfigFile.initialize();
    }

    @Before(order = 1)
    public void runBrowser(Scenario scenario) {
        String browserName = ConfigFile.browserName;
        ConfigFile.setDriver(browserName);
        startRecording(scenario);
    }

    @After(order = 1)
    public void closeBrowser() {
        quitBrowser();
    }

    @After(order = 1000)
    public void afterFail(Scenario scenario)
    {
        VedioConverter vedioConverter = new VedioConverter();
        byte[] readBytes = new byte[0];
        byte[] readBytes1 = new byte[0];
        if(scenario.isFailed()) {
            takeScreenshot(scenario);
            stopRecording();
            String mediaFile = vedioConverter.getConvertedvideo(videoPath+fileName+".mov", videoPath+fileName+".mp4");
            try {
                    readBytes = Files.readAllBytes(Paths.get(mediaFile));
                    readBytes1 = Files.readAllBytes((Paths.get(videoPath+fileName+".mov")));
            } catch (IOException e) {
                e.printStackTrace();
            }
            scenario.attach(readBytes,"video/mp4", scenario.getName());
            scenario.attach(readBytes1,"video/mp4", scenario.getName()+"1");
//            scenario.attach((videoPath+fileName+".mov").getBytes(),"image/mov", scenario.getName()+"1");
//            scenario.attach(videoPath+fileName,"image/mov", scenario.getName()+"vediofilemov");
//            scenario.attach(videoPath+fileName+".mov","image/mov", scenario.getName()+"vediofilemov1");
        }
    }

    public void startRecording(Scenario scenario)
    {
        DateFormat dateFormat = new SimpleDateFormat("yy-MM-dd HH-mm-ss");
        Date date = new Date();
        videoPath = System.getProperty("user.dir") + "/target/";
        fileName =  scenario.getName() + "-" + dateFormat.format(date);
        try{
            recorder = new ATUTestRecorder(videoPath, fileName, false);
        }
        catch (Exception e)
        {
            throw new ThrowException("failed initilization" + videoPath + fileName);
        }
        try{
            recorder.start();
        }
        catch (Exception e)
        {
            throw new ThrowException("failed recording");
        }
    }

    public void stopRecording()
    {
        try
        {
            recorder.stop();
        }
        catch (Exception e)
        {

        }
    }
}
