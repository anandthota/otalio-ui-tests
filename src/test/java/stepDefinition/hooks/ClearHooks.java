package stepDefinition.hooks;

import io.cucumber.java.After;
import stepDefinition.LoginSteps;
import stepDefinition.pos.ArticleSetupDefs;

public class ClearHooks {

    @After(value ="@clear_article")
    public void delete_artilce()
    {
        LoginSteps loginSteps = new LoginSteps();
        ArticleSetupDefs articleSetupDefs = new ArticleSetupDefs();
        loginSteps.iLoginOtalioAsUserAdmin();
        articleSetupDefs.iClickOnModule("pos module");
        articleSetupDefs.iClickOnArtilceSetupLink("link");
        articleSetupDefs.iSearchForArticle();
        try {
            articleSetupDefs.iClickOnArticle("");
            articleSetupDefs.iDeleteArticle("delete");
            articleSetupDefs.iWaitForArticleLoaded("dummy");
            articleSetupDefs.iDeleteArticle("dummy");
        }
        catch (Exception e)
        {
            //artilce not available possible reason 1) deleted by test 2) creation failed 3) search not success
        }
    }
}
