package stepDefinition.pos;

import com.core.BaseElements;
import com.pages.pos.ArticleSetupPage;
import com.utilities.ConfigFile;
import com.utilities.DataUtils;
import io.cucumber.java.en.And;
import io.cucumber.java.en.When;
import org.testng.Assert;

import java.util.Map;

public class ArticleSetupDefs {

    ArticleSetupPage posPage = new ArticleSetupPage();

    @When("I click on {string} module")
    public void iClickOnModule(String arg0) {
       posPage.clickOnPOSManagement();
    }

    @And("I click on {string} link")
    public void iClickOnArtilceSetupLink(String arg0) {
        posPage.clickOnArtilceSetup();
    }

    @And("I set values for article setup creation")
    public void iSetValuesForCreation(Map<String, String> values) {
        values= DataUtils.updatePlaceholderStringsOfMap(values);
        ConfigFile.articleName = values.get("Code");
        posPage.enterArticleName(ConfigFile.articleName);
        posPage.enterArticleDescription(values.get("Description"));
        posPage.selectArticleGroup(values.get("Atricle Group"));
    }

    @And("I click on new button")
    public void iClickOnNewButton() {
        posPage.clickOnNewArticleButton();
    }

    @And("I click on Create button")
    public void iClickOnButton() {
       posPage.clickOnCreateButton();
    }

    @And("^I verify article is \"(created|updated)\"$")
    public void iVerifyArticleCreationIsSuccess(String string) {
        Assert.assertEquals(ConfigFile.articleName.toUpperCase(), posPage.getArticleText(ConfigFile.articleName));
    }

    @And("^I verify article is deleted$")
    public void iVerifyArticleCreationIsDeleted() {
        posPage.clickOnCodeDropdown();
        posPage.enterCodeOnArticleSetupLandingPage(ConfigFile.articleName);
        Assert.assertTrue(posPage.articleNotDisaplayed(ConfigFile.articleName));
    }

    @And("I select {string} Revenue Center Category")
    public void iSelectRevenueCenterCategory(String revenue_center) {
        posPage.click_on_revenue_center();
        posPage.select_revenue_center_category(revenue_center);
    }

    @And("I click on article {string}")
    public void iClickOnArticle(String arg0) {
        posPage.clickOnArticle(ConfigFile.articleName);
    }

    @And("I click on Update button")
    public void iClickOnUpdateButton() {
        posPage.clickOnUpdateButton();
    }

    @And("I wait for article {string} loaded")
    public void iWaitForArticleLoaded(String string) {
        posPage.waitForArticleToLoad(ConfigFile.articleName);
    }

    @And("I delete article {string}")
    public void iDeleteArticle(String arg0) {
        posPage.clickOnDeleteArtilce();
    }

    @And("I search for article")
    public void iSearchForArticle() {
        posPage.clickOnCodeDropdown();
        posPage.enterCodeOnArticleSetupLandingPage(ConfigFile.articleName);
        posPage.waitForArticleToLoad(ConfigFile.articleName);
        posPage.closeOverlay();
    }
}
