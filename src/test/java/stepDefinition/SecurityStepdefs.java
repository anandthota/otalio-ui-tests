package stepDefinition;

import com.pages.LoginPage;
import com.pages.SecurityPage;
import com.pages.pos.ArticleSetupPage;
import io.cucumber.java.en.And;
import org.testng.Assert;

public class SecurityStepdefs {

    SecurityPage securityPage = new SecurityPage();
    LoginPage loginPage = new LoginPage();
    ArticleSetupPage articleSetupPage = new ArticleSetupPage();

    @And("^click on resources \"([^\"]*)\" on pages \"([^\"]*)\" and save with type as Frontend$")
    public void clickOnResourcesAndSaveWithTypeAsFrontend(String resources, String Totalpages) {
//        int resources = 1598;
//        int Totalpages = 80;
        int row = 130;
        int tableRowInPage = row % 20 ;

        String resourceName = null;
        String permissionNameOnLandingPage = null;
        for (int page = 341; page <= Integer.parseInt(Totalpages); page++) {

            System.out.println("Now we are at page :" + page);
            for (; row <= Integer.parseInt(resources); row++) {
                try {
                    loginPage.navigateToUrl("https://seed-central.otaliodev.com/security/resources");
                    iClickOnTypeFronend();
                    Thread.sleep(2000);
                    articleSetupPage.closeOverlay();
                    iclickOnPageNavigation(page);
                    iClickOnResource(row, tableRowInPage);
                    Thread.sleep(2000);
                    permissionNameOnLandingPage = securityPage.getResourceNameOnLandingPage(row);
                    resourceName = iGetResourceName();
                    iClickOnSave();
                    iVerifyResourceisSaved(resourceName,permissionNameOnLandingPage, row,page);
                } catch (Exception e) {
                    System.out.println("save resources is failed at index:" + row +" and name is:" + resourceName);
                }
                finally {
                    tableRowInPage++;
                    if (row % 20 == 0) {
                        tableRowInPage = 1;
                        break;
                    }
                }
            }
            row++;
        }
    }

    private void iVerifyResourceisSaved(String resourceName,
                                        String actualMessage, int row, int page) {
//        iclickOnPageNavigation(page);
//        try {
//            Thread.sleep(2000);
//        } catch (InterruptedException e) {
//            e.printStackTrace();
//        }
        //String actualmessage = securityPage.getResourceNameOnLandingPage(row);
        if(resourceName.equals(actualMessage));
             System.out.println("save resources is passed at index:" + row +" and name is:" + actualMessage);
    }

    private String iGetResourceName() {
        return securityPage.getResourceName();
    }

    private void iscrollToTableGrid() {
        securityPage.scrollToTableGrid();
    }

    private void iclickOnPageNavigation(int number) {

            for(int j = 5; j< number; j++ )
                securityPage.clickOnPage(j);

        securityPage.clickOnPage(number);
    }

    public void iClickOnResource(int row, int count) {
        securityPage.clickOnResource(row,count);
    }

    public void iClickOnTypeFronend() {
        securityPage.clickOnTypeFronend();
    }

    public void iClickOnApplicationSecurity() {
        securityPage.clickOnApplicationSecurity();
    }

    public void iClickOnSave() {
        securityPage.clickOnSave();
    }

    public void iClickOnPermissoinsSave() {
        securityPage.clickOnPermissoinsSave();
    }

    @And("click on permissions {string} on pages {string} and save with type as Frontend")
    public void clickOnPermissionsOnPagesAndSaveWithTypeAsFrontend(String resources, String Totalpages) {

        int row = 801;
       // int tableRowInPage = row % 20 ;

        String resourceName = null;
        String permissionNameOnLandingPage = null;
        for (int page = 41; page <= Integer.parseInt(Totalpages); page++) {

            System.out.println("Now we are at page :" + page);
            for (; row <= Integer.parseInt(resources); row++) {
                try {
                    loginPage.navigateToUrl("https://shore-dev.otaliodev.com/security/permissions");
                    Thread.sleep(1000);
                    iclickOnPageNavigation(page);
                    permissionNameOnLandingPage = securityPage.getResourceNameOnLandingPage(row);
                    iClickOnNameDropdown();
                    ienterpermissonName(permissionNameOnLandingPage);
                    Thread.sleep(2000);
                    articleSetupPage.closeOverlay();
                    iClickOnpermissionName(permissionNameOnLandingPage );
                    //iClickOnResource(row, tableRowInPage);
                    Thread.sleep(2000);
                    resourceName = iGetResourceName();
                    iClickOnPermissoinsSave();
                    Thread.sleep(2000);
                    iVerifyResourceisSaved(resourceName, permissionNameOnLandingPage, row,page);
                } catch (Exception e) {
                    System.out.println("save resources is failed at index:" + row +" and name is:" + permissionNameOnLandingPage);
                }
                finally {
                   // tableRowInPage++;
                    if (row % 20 == 0) {
                        //tableRowInPage = 1;
                        break;
                    }
                }
            }
            row++;
        }
    }

    public void iClickOnNameDropdown() {
        securityPage.clickOnNameDropdown();
    }

    public void ienterpermissonName(String permissionNameOnLandingPage){
        securityPage.enterDataToPermissionName(permissionNameOnLandingPage);
    }

    public void iClickOnpermissionName(String permissionNameOnLandingPage)
    {
        securityPage.clickOnPermissionName(permissionNameOnLandingPage);
    }
}
