package stepDefinition;

import com.core.BasePage;
import com.core.DriverManager;
import com.pages.LoginPage;
import io.cucumber.java.en.And;
import io.cucumber.java.en.Given;
import io.cucumber.java.en.Then;
import io.cucumber.java.en.When;
import org.testng.Assert;

import static com.utilities.ConfigFile.password;
import static com.utilities.ConfigFile.userName;

public class LoginSteps  {

    LoginPage loginPage = new LoginPage();

    @Given("^I Open a Browser$")
    public void iOpenABrowser() {
        loginPage.openBrowser();
    }

    @And("I navigate to url {string}")
    public void iNavigateToUrl(String url) {
        loginPage.navigateToUrl(url);
    }

    @When("^I enter a user admin$")
    public void iEnterAUserAdmin() {
        loginPage.enterAdminUserName(userName);
    }

    @And("^I enter a user password$")
    public void iEnterAUserPassword() {
        loginPage.enter_password(password);
    }

    @And("^I click on login button$")
    public void iClickOnLoginButton() {
        loginPage.click_on_login_button();
    }

    @And("^I click on user profile$")
    public void iClickOnUserProfile() {
        loginPage.click_on_user_profile();
    }



    @And("^I click on personal information$")
    public void iClickOnPersonalInformation() {
        loginPage.click_on_personal_information_button();
    }

    @Then("^I verify user displayed is Super User$")
    public void iVerifyUserDisplayedIsSuperUser() {
        Assert.assertEquals(loginPage.get_user_loged_in_name().trim(), "Super User");
    }

    @Given("I login Otalio as user admin")
    public void iLoginOtalioAsUserAdmin() {
        loginPage.openBrowser();
        loginPage.enterAdminUserName(userName);
        loginPage.enter_password(password);
        loginPage.click_on_login_button();
        loginPage.wait_for_user_profile_to_load();
    }


}
