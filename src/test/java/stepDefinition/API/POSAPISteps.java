package stepDefinition.API;

import com.API.RestAPICall;
import io.cucumber.java.en.And;
import io.cucumber.java.en.Given;

import static com.utilities.ConfigFile.restAPICall;

public class POSAPISteps {

    @Given("^I request \"([^\"]*)\" call \"([^\"]*)\" for pos article-categoires$")
    public void iApi_methodRequestForModuleWithUserPassword(String requestType, String uri)  {
        restAPICall.getResponse(uri);
    }

}
