package stepDefinition.API;

import com.API.RestAPICall;
import com.utilities.ConfigFile;
import io.cucumber.java.en.Given;
import io.cucumber.java.en.Then;
import org.testng.Assert;

import static com.utilities.ConfigFile.restAPICall;

public class GenericAPISteps {

    @Given("^I generate token for \"([^\"]*)\"$")
    public void iGenerateTokenFor(String user) {
        restAPICall.setUser(user);
    }

    @Then("^I verify \"([^\"]*)\" response \"([^\"]*)\"$")
    public void iVerifyResponseAsResponseMessage(String key, String message) {
        Assert.assertEquals(String.valueOf(restAPICall.getPostResponseBody(key)), message);
    }

    @Then("^I verify response_code as \"([^\"]*)\"$")
    public void iVerifyResponseCode(String responseStatus) {
        Assert.assertEquals(String.valueOf(restAPICall.getStatusCode()), responseStatus);
    }

}
